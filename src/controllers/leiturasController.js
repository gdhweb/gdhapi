import leituras from "../models/Leitura.js";
import observadores from "../models/Observador.js";

class LeituraController{

    static listarLeituras = (req, res) => {
            leituras.find()
                .populate('observador', 'nome')
                .exec((err, leituras) => {
                if(err){
                    res.status(400).send({message: `${err.message} - Erro ao Listar Leituras.`})
                } else {
                    res.status(200).send(leituras);
                }
            })
    }

    static listarLeituraPorId = (req, res) => {
        const id = req.params.id;
        leituras.findById(id)
            .populate('observador')
            .exec((err, leituras) => {
            if(err){
                res.status(400).send({message: `${err.message} - Id da Leitura não localizado.`})
            } else {
                res.status(200).send(leituras);
            }
        })
    }

    static cadastrarLeitura = (req, res) => {
        let leitura = new leituras(req.body);
        leitura.save((err) => {
            if(err) {
                res.status(500).send({message: `${err.message} - Falha ao cadastrar Leitura`})
            } else{
                res.status(201).send(leitura.toJSON())
            }
        })
    }
    
    static atualizarLeitura = (req, res) => {
        const id = req.params.id;
        leituras.findByIdAndUpdate(id, {$set: req.body}, (err) => {
            if(!err) {
                res.status(200).send({message: 'Leitura atualizada com sucesso'})
            } else {
                res.status(500).send({message: err.message})
            }
        })
    }

    static excluirLeitura = (req, res) => {
        const id = req.params.id;
        leituras.findByIdAndDelete(id, (err) => {
            if(!err){
                res.status(200).send('Leitura removido com sucesso')
            } else {
                res.status(500).send({message: err.message})
            }
        })
    }

    static listarLeituraPorEstacao = (req, res) => {
        const estacao = req.query.estacao

        leituras.find({'estacao': estacao}, {}, (err, leituras) => {
            res.status(200).send(leituras);
        })
    }
}

export default LeituraController