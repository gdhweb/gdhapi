import observadores from "../models/Observador.js";

class ObservadorController{

    static listarObservadores = (req, res) => {
            observadores.find((err, observadores) => {
                res.status(200).json(observadores)
            })
    }

    static listarObservadorPorId = (req, res) => {
        const id = req.params.id;
        observadores.findById(id, (err, observadores) => {
            if(err){
                res.status(400).send({message: `${err.message} - Id do Observador não localizado.`})
            } else {
                res.status(200).send(observadores);
            }
        })
    }

    static cadastrarObservador = (req, res) => {
        let observador = new observadores(req.body);
        observador.save((err) => {
            if(err) {
                res.status(500).send({message: `${err.message} - Falha ao cadastrar observador`})
            } else{
                res.status(201).send(observador.toJSON())
            }
        })
    }
    
    static atualizarObservador = (req, res) => {
        const id = req.params.id;
        observadores.findByIdAndUpdate(id, {$set: req.body}, (err) => {
            if(!err) {
                res.status(200).send({message: 'Observador atualizado com sucesso'})
            } else {
                res.status(500).send({message: err.message})
            }
        })
    }

    static excluirObservador = (req, res) => {
        const id = req.params.id;
        observadores.findByIdAndDelete(id, (err) => {
            if(!err){
                res.status(200).send('Observador removido com sucesso')
            } else {
                res.status(500).send({message: err.message})
            }
        })
    }
}

export default ObservadorController