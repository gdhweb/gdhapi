import express from "express";
import ObservadorController from "../controllers/observadoresController.js";

const router = express.Router();

router
    .get("/observadores", ObservadorController.listarObservadores)
    .get("/observadores/:id", ObservadorController.listarObservadorPorId)
    .post("/observadores", ObservadorController.cadastrarObservador)
    .put("/observadores/:id", ObservadorController.atualizarObservador)
    .delete("/observadores/:id", ObservadorController.excluirObservador) 

export default router;