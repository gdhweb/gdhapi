import express from "express";
import LeituraController from "../controllers/leiturasController.js";

const router = express.Router();

router
    .get("/leituras", LeituraController.listarLeituras)
    .get("/leituras/busca", LeituraController.listarLeituraPorEstacao)
    .get("/leituras/:id", LeituraController.listarLeituraPorId)
    .post("/leituras", LeituraController.cadastrarLeitura)
    .put("/leituras/:id", LeituraController.atualizarLeitura)
    .delete("/leituras/:id", LeituraController.excluirLeitura) 

export default router;