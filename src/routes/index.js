import express from "express";
import leituras from "./leiturasRoutes.js";
import observadores from "./observadoresRouter.js";

const routes = (app) => {
    app.route('/').get((req, res) => {
        res.status(200).send({titulo: "API de Leituras e Obvervadores do GDH Web - HOMEPAGE"})
    })
    app.use(
        express.json(),
        leituras,
        observadores
    )
}

export default routes