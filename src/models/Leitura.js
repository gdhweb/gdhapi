import mongoose from "mongoose";

const leituraSchema = new mongoose.Schema (
    {
    observador: {type: mongoose.Schema.Types.ObjectId, ref: 'observadores', required: true},
    leitura: {type: String, required: true},
    estacao: {type: String, required: true},
    dialeitura: {type: Date, required: true},
    horaleitura: {type: String, required: true}
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

const leituras = mongoose.model('leituras', leituraSchema);

export default leituras;