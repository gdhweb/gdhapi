import mongoose from "mongoose";

const observadorSchema = new mongoose.Schema (
    {
        nome: {type: String, required: true},
        cpf: {type: String, required: true},
        email: {type: String, required: true},
        org: {type: String, required: true}
    },
    {
        versionKey: false,
    }
)

const observadores = mongoose.model("observadores", observadorSchema)

export default observadores;